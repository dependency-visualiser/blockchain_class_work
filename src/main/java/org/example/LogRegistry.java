package org.example;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.4.2.
 */
@SuppressWarnings("rawtypes")
public class LogRegistry extends Contract {
    public static final String BINARY = "60806040523480156100115760006000fd5b505b33600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505b61005a565b610e7d806100696000396000f3fe60806040526004361061004e5760003560e01c8063169557ef1461005d5780634a35ebe51461014557806381b2248a146102a6578063b2494df31461030d578063f851a4401461037b57610057565b36610057575b5b005b60006000fd5b34801561006a5760006000fd5b50610143600480360360608110156100825760006000fd5b8101908080359060200190929190803560ff169060200190929190803590602001906401000000008111156100b75760006000fd5b8201836020820111156100ca5760006000fd5b803590602001918460018302840111640100000000831117156100ed5760006000fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f820116905080830192505050505050509090919290909192905050506103bd565b005b6102a46004803603604081101561015c5760006000fd5b810190808035906020019064010000000081111561017a5760006000fd5b82018360208201111561018d5760006000fd5b803590602001918460018302840111640100000000831117156101b05760006000fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f82011690508083019250505050505050909091929090919290803590602001906401000000008111156102185760006000fd5b82018360208201111561022b5760006000fd5b8035906020019184600183028401116401000000008311171561024e5760006000fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f82011690508083019250505050505050909091929090919290505050610541565b005b3480156102b35760006000fd5b506102e1600480360360208110156102cb5760006000fd5b810190808035906020019092919050505061072a565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561031a5760006000fd5b5061032361076f565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b838110156103675780820151818401525b60208101905061034b565b505050509050019250505060405180910390f35b3480156103885760006000fd5b50610391610805565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b6002600050600084815260200190815260200160002060005060405180606001604052808460028111156103ed57fe5b815260200183815260200142815260200150908060018154018082558091505060019003906000526020600020906003020160005b9091909190915060008201518160000160006101000a81548160ff0219169083600281111561044d57fe5b0217905550602082015181600101600050908051906020019061047192919061082b565b5060408201518160020160005090905550507fc186aa46c62458107df79771967f511ff20bdd52309cdc80594a838b994da5cf828242604051808460028111156104b757fe5b815260200180602001838152602001828103825284818151815260200191508051906020019080838360005b838110156104ff5780820151818401525b6020810190506104e3565b50505050905090810190601f16801561052c5780820380516001836020036101000a031916815260200191505b5094505050505060405180910390a15b505050565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156105ae5764e8d4a5100034101515156105ad5760006000fd5b5b600082826040516105be906108b0565b808060200180602001838103835285818151815260200191508051906020019080838360005b838110156106005780820151818401525b6020810190506105e4565b50505050905090810190601f16801561062d5780820380516001836020036101000a031916815260200191505b50838103825284818151815260200191508051906020019080838360005b838110156106675780820151818401525b60208101905061064b565b50505050905090810190601f1680156106945780820380516001836020036101000a031916815260200191505b50945050505050604051809103906000f0801580156106b8573d600060003e3d6000fd5b509050600160005081908060018154018082558091505060019003906000526020600020900160005b9091909190916101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550505b5b5050565b60016000508181548110151561073c57fe5b906000526020600020900160005b9150909054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b606060016000508054806020026020016040519081016040528092919081815260200182805480156107f657602002820191906000526020600020905b8160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190600101908083116107ac575b50505050509050610802565b90565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061086c57805160ff191683800117855561089f565b8280016001018555821561089f579182015b8281111561089e578251826000509090559160200191906001019061087e565b5b5090506108ac91906108bd565b5090565b610567806108e183390190565b6108c2565b808211156108dc57600081815060009055506001016108c2565b509056fe60806040523480156100115760006000fd5b50604051610567380380610567833981810160405260408110156100355760006000fd5b81019080805160405193929190846401000000008211156100565760006000fd5b8382019150602082018581111561006d5760006000fd5b825186600182028301116401000000008211171561008b5760006000fd5b8083526020830192505050908051906020019080838360005b838110156100c05780820151818401525b6020810190506100a4565b50505050905090810190601f1680156100ed5780820380516001836020036101000a031916815260200191505b50604052602001805160405193929190846401000000008211156101115760006000fd5b838201915060208201858111156101285760006000fd5b82518660018202830111640100000000821117156101465760006000fd5b8083526020830192505050908051906020019080838360005b8381101561017b5780820151818401525b60208101905061015f565b50505050905090810190601f1680156101a85780820380516001836020036101000a031916815260200191505b506040526020015050505b81600060005090805190602001906101cc9291906101ef565b5080600160005090805190602001906101e69291906101ef565b505b5050610297565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061023057805160ff1916838001178555610263565b82800160010185558215610263579182015b828111156102625782518260005090905591602001919060010190610242565b5b5090506102709190610274565b5090565b610279565b808211156102935760008181506000905550600101610279565b5090565b6102c1806102a66000396000f3fe60806040523480156100115760006000fd5b506004361061003b5760003560e01c806306fdde03146100415780637284e416146100c55761003b565b60006000fd5b610049610149565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561008a5780820151818401525b60208101905061006e565b50505050905090810190601f1680156100b75780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6100cd6101ea565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561010e5780820151818401525b6020810190506100f2565b50505050905090810190601f16801561013b5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b60006000508054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156101e25780601f106101b7576101008083540402835291602001916101e2565b820191906000526020600020905b8154815290600101906020018083116101c557829003601f168201915b505050505081565b60016000508054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102835780601f1061025857610100808354040283529160200191610283565b820191906000526020600020905b81548152906001019060200180831161026657829003601f168201915b50505050508156fea2646970667358221220a295254594bab5fa381d17cc3f329328a84a84fd31e514d3ad5501a24b60b9d064736f6c63430007030033a26469706673582212206acf9890100a2df13c30af982dce9e751f26f22fc2f317935ab73aedd858e95064736f6c63430007030033";

    public static final String FUNC_ADDLOGS = "addLogs";

    public static final String FUNC_ADDMODULES = "addModules";

    public static final String FUNC_ADMIN = "admin";

    public static final String FUNC_GETMODULES = "getModules";

    public static final String FUNC_MODULES = "modules";

    public static final Event NEWLOG_EVENT = new Event("NewLog",
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint8>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}));
    ;

    @Deprecated
    protected LogRegistry(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected LogRegistry(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected LogRegistry(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected LogRegistry(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static List<NewLogEventResponse> getNewLogEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = staticExtractEventParametersWithLog(NEWLOG_EVENT, transactionReceipt);
        ArrayList<NewLogEventResponse> responses = new ArrayList<NewLogEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            NewLogEventResponse typedResponse = new NewLogEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.state = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.text = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.tume = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<NewLogEventResponse> newLogEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, NewLogEventResponse>() {
            @Override
            public NewLogEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(NEWLOG_EVENT, log);
                NewLogEventResponse typedResponse = new NewLogEventResponse();
                typedResponse.log = log;
                typedResponse.state = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.text = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.tume = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<NewLogEventResponse> newLogEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWLOG_EVENT));
        return newLogEventFlowable(filter);
    }

    public RemoteFunctionCall<TransactionReceipt> addLogs(BigInteger moduleId, BigInteger state, String text) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_ADDLOGS,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(moduleId),
                        new org.web3j.abi.datatypes.generated.Uint8(state),
                        new org.web3j.abi.datatypes.Utf8String(text)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> addModules(String name, String description, BigInteger weiValue) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_ADDMODULES,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(name),
                        new org.web3j.abi.datatypes.Utf8String(description)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteFunctionCall<String> admin() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_ADMIN,
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<List> getModules() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETMODULES,
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Address>>() {}));
        return new RemoteFunctionCall<List>(function,
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public RemoteFunctionCall<String> modules(BigInteger param0) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_MODULES,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    @Deprecated
    public static LogRegistry load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new LogRegistry(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static LogRegistry load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new LogRegistry(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static LogRegistry load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new LogRegistry(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static LogRegistry load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new LogRegistry(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<LogRegistry> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(LogRegistry.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<LogRegistry> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(LogRegistry.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<LogRegistry> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(LogRegistry.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<LogRegistry> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(LogRegistry.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static class NewLogEventResponse extends BaseEventResponse {
        public BigInteger state;

        public String text;

        public BigInteger tume;
    }
}
