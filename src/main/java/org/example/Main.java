package org.example;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.tx.gas.StaticGasProvider;

import java.io.IOException;
import java.math.BigInteger;

public class Main {
    public static void main(String[] args) throws Exception {
        Web3j web3 = Web3j.build(new HttpService("https://goerli.infura.io/v3/8a2d71ec92784b49a1e76817369de213"));
//        BigInteger blockNumber = web3j.ethBlockNumber().send().getBlockNumber();
//        System.out.println(blockNumber);
//        System.out.println("Hello world");
        Credentials credentials = loadCredentials("35e25e83c7ef5202ec304c1672144f16a530b87c0f153f38c6d00123f8ded59b");
        System.out.println(credentials.getAddress());
//        String address = ; //
//        LogRegistry contract = loadConstract(web3, address, credentials);
//        System.out.println(contract.admin().send());
        LogRegistry registry = deployContract(web3, credentials);
        System.out.println(registry.getContractAddress());

        TransactionReceipt receipt = registry.addModules("Module 0", "Test description", BigInteger.ZERO).send();
        System.out.println(receipt.getTransactionHash()); // брать хэш для дальнейшего использования, брать информацию не тянув целый контракт?? вроде как

    }

    public static LogRegistry deployContract(Web3j web3, Credentials credentials) throws Exception {
        ContractGasProvider gasProvider = new StaticGasProvider(BigInteger.valueOf(90000000000L), BigInteger.valueOf(2500000L));
        return LogRegistry.deploy(web3, credentials, gasProvider).send();
    }

    public static LogRegistry loadContract(Web3j web3, String address, Credentials credentials) {
        ContractGasProvider gasProvider = new DefaultGasProvider();
        LogRegistry contract = LogRegistry.load(address, web3, credentials, gasProvider);
        return contract;
    }


    public static void getBlockInfo(Web3j web3, BigInteger blockNumber) throws IOException {
        EthBlock.Block ethBlockRequest = web3.ethGetBlockByNumber(
                DefaultBlockParameter.valueOf(blockNumber),
                true).send().getBlock(); // true: тянем весь заголовок и доп инфу, false: не тянем
    }

    public static Credentials loadCredentials(String privateKey) {
        return Credentials.create(privateKey);
    }
}
// ДЗ:
//
// 2) Реализовать функцию, которая принимает на вход номер стартвого блока,
// номер конечного блока, адрес отправителя и возвращает
// список транзакций этого адреса
// 3) реализовать свой газ провайдер, который подтягивает актуальный gas price
// через API
// у меня в итоге не получилось задеплоить: после 600 секунд (т.к. установлен неявный тайм-аут) программа завершилась
// в принципе всё)))